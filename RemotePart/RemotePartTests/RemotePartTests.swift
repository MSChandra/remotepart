//
//  RemotePartTests.swift
//  RemotePartTests
//
//  Created by CHANDRA SEKARAN M on 03/07/2017.
//  Copyright © 2017 CHANDRA SEKARAN M. All rights reserved.
//

import XCTest
@testable import RemotePart

class RemotePartTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPlaceList() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let expectation0 = expectation(description: "Place retrieved")
        
        let mainViewModelSuccess =  MainViewModelSuccess()
        
        mainViewModelSuccess.testSearchPlaces(forQuery:"London", successCall: { (data) in
            XCTAssert(mainViewModelSuccess.getResultForList().count != 0 , "Place retrival failed")
            expectation0.fulfill()
        }, failureCall:{ (err) in
            if let error = err {
                XCTFail("Places retrival failed -> \(error)")
            }
        })
        
        waitForExpectations(timeout: 500) { error in
            if let error = error {
                XCTFail("Timeout err -> \(error)")
            }
        }
        
    }
    
    func testNearByPlaceList() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let expectation0 = expectation(description: "Place retrieved")
        
        let mainViewModelSuccess =  MainViewModelSuccess()
        //51.5194652550583
        //0.0594916372426717
        mainViewModelSuccess.testSearchNearbyPlaces(lng:"0.0594916372426717", lat:"51.5194652550583", successCall: { (data) in
            XCTAssert(mainViewModelSuccess.getResultForMap().count != 0 , "Place retrival failed")
            expectation0.fulfill()
        }, failureCall:{ (err) in
            if let error = err {
                XCTFail("Places retrival failed -> \(error)")
            }
        })
        
        waitForExpectations(timeout: 500) { error in
            if let error = error {
                XCTFail("Timeout err -> \(error)")
            }
        }
        
    }
    
    func testPlaceDetails() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let expectation0 = expectation(description: "Product Details retrieved")
        
        let placeDetailsViewModelSuccess =  PlaceDetailsViewModelSuccess()
        
        placeDetailsViewModelSuccess.testLoadPlaceDetails(placeId:"ChIJdd4hrwug2EcRmSrV3Vo6llI", successCall: { (data) in
            XCTAssert(placeDetailsViewModelSuccess.datatToShow() != nil , "Place Details retrival failed")
            expectation0.fulfill()
        }, failureCall:{ (err) in
            if let error = err {
                XCTFail("Place Details retrival failed -> \(error)")
            }
        })
        
        waitForExpectations(timeout: 500) { error in
            if let error = error {
                XCTFail("Timeout err -> \(error)")
            }
        }
        
    }
    
}


class MainViewModelSuccess : MainViewModel {
    
    fileprivate var searchText : String?
    
    
    var success: (SuccessJSONBlock)?
    var failure: (FailureBlock)?
    
    func testSearchPlaces(forQuery:String, successCall:  @escaping (JSONDict?) -> Void , failureCall:  @escaping (ResponseError?) -> Void ){
        self.searchText = forQuery
        self.success = successCall
        self.failure = failureCall
        self.searchPlaces(forQuery: forQuery)
    }
    func testSearchNearbyPlaces( lng:String, lat:String, successCall:  @escaping (JSONDict?) -> Void , failureCall:  @escaping (ResponseError?) -> Void ){
        self.searchText = "bar"
        self.success = successCall
        self.failure = failureCall
        let service = PlacesService(success: searchPlacesCallSuccess(),failure: searchPlacesCallFail() )
        service.searchNearBy(placeType: "bar", longitude: lng, latitude: lat)
    }
    
    
    override func searchPlacesCallSuccess() -> SuccessJSONBlock {
        return {
            [weak self](json) in
            
            if let query = self?.searchText {
                let _ = DBClient.sharedInstance.addSearchResult(forQuery: query, data: PlaceMetaData.createPlaces(fromJson: json))
            }
            
            if let successCall = self?.success {
                successCall(json)
            }
        }
    }
    override func searchPlacesCallFail() -> FailureBlock  {
        
        return {[weak self] (error) in
            if let failCall = self?.failure {
                failCall(error)
            }
        }
        
    }
}

class PlaceDetailsViewModelSuccess : PlaceDetailsViewModel {
    
    var success: (SuccessJSONBlock)?
    var failure: (FailureBlock)?
    
    
    func testLoadPlaceDetails(placeId:String, successCall:  @escaping (JSONDict?) -> Void , failureCall:  @escaping (ResponseError?) -> Void ){
        self.success = successCall
        self.failure = failureCall
        self.fetchPlaceDetails(byId: placeId)
    }
    
    override func searchPlaceDetailsCallSuccess() -> SuccessJSONBlock {
        return {
            [weak self](json) in
            
            self?.details = PlaceMetaData.createPlacesDetail(fromJson: json)
            
            if let successCall = self?.success {
                successCall(json)
            }
        }
    }
    override func searchPlaceDetailsCallFail() -> FailureBlock  {
        
        return {[weak self] (error) in
            if let failCall = self?.failure {
                failCall(error)
            }
        }
        
    }
}
