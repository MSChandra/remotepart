//
//  PlaceMetaData.swift
//  RemotePart
//
//  Created by CHANDRA SEKARAN M on 03/07/2017.
//  Copyright © 2017 CHANDRA SEKARAN M. All rights reserved.
//

import Foundation

class PlaceMetaData {

    var placeId: String?
    var name : String?
    var imageUrl : String?
    var types: [String]?
    var address: String?
    var photoReferences : [String] = [] //Banu
    var reviews = [String]()
    
    var location: (longitude:NSNumber, latitude: NSNumber)?
    
    static func createPlaces(fromJson:JSONDict?) ->[PlaceMetaData] {
        var list  = [PlaceMetaData]()
        
        guard let json = fromJson, let jsonList = json["results"] as? [JSONDict] else {
            return list
        }
        
        for placeDict in jsonList {
            let placeObj = createPlace(fromJSON: placeDict)
     
            list.append(placeObj)
        }
        
        return list
    }
    
    static func createPlacesDetail(fromJson:JSONDict?) ->PlaceMetaData? {
        guard let json = fromJson, let jsonDict = json["result"] as? JSONDict else {
            return nil
        }
        
        return createPlace(fromJSON: jsonDict)
    }
    
    static func createPlace(fromJSON:JSONDict) -> PlaceMetaData {
    let place = PlaceMetaData()
        
        place.placeId = fromJSON["place_id"] as? String
        place.name = fromJSON["name"] as? String
        place.imageUrl = fromJSON["icon"] as? String
        place.address = (fromJSON["formatted_address"] as? String) ??  (fromJSON["vicinity"] as? String)
        if let geo = fromJSON["geometry"] as? JSONDict, let loc = geo["location"] as? JSONDict,
            let lng = loc["lng"] as? NSNumber,
            let lat = loc["lat"] as? NSNumber {
            place.location = (longitude:lng, latitude:lat)
        }
        //Banu
        if let photoInfos = fromJSON["photos"] as? [JSONDict]{
           // print(photoInfos)
            for photoInfo in photoInfos{
                
                if let photoRef = photoInfo["photo_reference"] as? String{
                    place.photoReferences.append(photoRef)
                 //   print("photoreference inside placemetadata class:-:)")
                  //  print(place.photoReference)

                }
            
            }
            
        }
        if let reviewDicts = fromJSON["reviews"] as? [JSONDict] {
            for review in reviewDicts {
                if let text =  review["text"] as? String{
                    place.reviews.append(text)
                }
            }
        }
        
        place.types = fromJSON["types"] as? [String]
        return place
        
    }
}
