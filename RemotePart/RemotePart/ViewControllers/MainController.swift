//
//  MainViewController.swift
//  RemotePart
//
//  Created by CHANDRA SEKARAN M on 03/07/2017.
//  Copyright © 2017 CHANDRA SEKARAN M. All rights reserved.
//

import UIKit

class MainViewController: BaseViewController, MainViewProtocol{
    
    fileprivate var tabs: UISegmentedControl!
    private var pageViewController : UIPageViewController!
    
    @IBOutlet weak var activityInd:UIActivityIndicatorView!
    @IBOutlet weak var resultsView:UIView!
    @IBOutlet weak var searchBar : UISearchBar!
    
    fileprivate var resultViewsList = [UIViewController]()
    
    var mPresenter = MainPresenter()
    var resultListCntrl = ResultsListViewController()
    var mapViewCntrl = MapViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBar.topItem?.title = " "
        mPresenter.mainDelegate = self
        tabs = UISegmentedControl(items: ["List", "Map"])
        tabs.sizeToFit()
        tabs.tintColor = UIColor(red:0.99, green:0.00, blue:0.25, alpha:1.00)
        tabs.selectedSegmentIndex = 0
        navigationItem.titleView = tabs
        
        if let list = self.viewController(withIdentifier:"List") as? ResultsListViewController {
           list.viewPresenter = self.mPresenter
            list.navigationDelegate = self
            resultViewsList.append(list)
        }
        if let map = self.viewController(withIdentifier:"Map") as? MapViewController {
           map.viewPresenter = self.mPresenter
            map.navigationDelegate = self
            resultViewsList.append(map)
        }
        searchBar.delegate = self
    
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.dataSource = self
        pageViewController.view.frame = CGRect(x: 0, y: 0, width: resultsView.frame.size.width, height: resultsView.frame.size.height)
        self.setupPageControl()

        if let listView =  resultViewsList.first {
            let viewControllers = [listView]
            pageViewController.setViewControllers(viewControllers , direction: .forward, animated: false, completion: nil)
            resultsView.addSubview(pageViewController.view)
        }
        tabs.addTarget(self, action: #selector(tabChanged), for:.valueChanged)
        activityInd.stopAnimating()
        
       // self.notify(name: Constants.fetchPlacesSuccessNotification, selector: #selector(MainViewController.refreshResults))
       // self.notify(name: Constants.fetchPlacesFailNotification, selector: #selector(MainViewController.showErr))
    }
    
    @objc func tabChanged(segment: UISegmentedControl) {
        
        if segment.selectedSegmentIndex == 0 {
            if let listView =  resultViewsList.first, resultViewsList.count == 2 {
                let viewControllers = [listView]
                pageViewController.setViewControllers(viewControllers , direction: .forward, animated: false, completion: nil)
            }
        }else{
            if let mapView =  resultViewsList.last, resultViewsList.count == 2 {
                let viewControllers = [mapView]
                pageViewController.setViewControllers(viewControllers , direction: .forward, animated: false, completion: nil)
            }
        }
        
    }
    
    @IBAction func searchPubs(_ sender: Any) {
        mPresenter.searchNearByPubs()
    }
    
    @objc func refreshResults(){
        self.activityInd.stopAnimating()
        //mPresenter.postNotification(name: Constants.placesRefreshNotification, info: nil)
        resultListCntrl.reloadData()
        mapViewCntrl.reloadData()
    }
    
    @objc func showErr(){
        
        self.activityInd.stopAnimating()
        self.showError(withMessage: "StartupFail".localizedText())
        
    }
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.white
        appearance.currentPageIndicatorTintColor = UIColor.yellow
        appearance.backgroundColor = UIColor.lightGray
    }

}

extension MainViewController : UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        if resultViewsList.last == viewController {
            self.tabs.selectedSegmentIndex = 0
            return resultViewsList.first
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        if resultViewsList.first == viewController {
            self.tabs.selectedSegmentIndex = 1
            return resultViewsList.last
        }
        
        return nil
    }
    
}

extension MainViewController : UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        if let text = searchBar.text {
            self.activityInd.layer.zPosition = 1
            self.activityInd.startAnimating()
            mPresenter.searchPlaces(forQuery:text)
        }
    }
    
}

extension MainViewController : NavigationDelegate {
    
    func navigateTo(view viewController:UIViewController) {
         self.view.endEditing(true)
         self.navigationController?.pushViewController(viewController, animated: false)
    }
    
}
