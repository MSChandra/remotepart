//
//  PhotoPageViewController.swift
//  RemotePart
//
//  Created by mbanu on 13/09/18.
//  Copyright © 2018 CHANDRA SEKARAN M. All rights reserved.
//

import UIKit
class PhotoPageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
   lazy var subViewControllers:[UIViewController] = {
       // return [UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhotoPageContentViewController") as! PhotoPageContentViewController]
        let sb = UIStoryboard(name: "Main", bundle: nil)
   var subViewControllers = [UIViewController]()
    self.phReferences?.forEach({ (photo) in
        let vc = sb.instantiateViewController(withIdentifier: "PhotoPageContentViewController") as! PhotoPageContentViewController
        vc.id = place
        vc.photoRef = photo.photoDetail
        subViewControllers.append(vc)
    })
    
    return subViewControllers
    
    }()
    
    var place:String?
    var phReferences:[Photo]?
    var pageControl = UIPageControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        dataSource = self
         setViewControllers([subViewControllers[0]], direction: .forward, animated: true, completion: nil)
        configurePageControl()
    }
    func configurePageControl(){
        pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY - 50, width: UIScreen.main.bounds.width, height: 50))
        pageControl.numberOfPages = subViewControllers.count
        pageControl.currentPage = 0
        pageControl.tintColor = UIColor.black
        pageControl.pageIndicatorTintColor = UIColor.white
        pageControl.currentPageIndicatorTintColor = UIColor.black
        self.view.addSubview(pageControl)
        
    }
        // Do any additional setup after loading the view.
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = subViewControllers.index(of: pageContentViewController)!
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let currentIndex = subViewControllers.index(of: viewController){
            if currentIndex > 0{
                return subViewControllers[currentIndex-1]
            }
        }
        return nil
        
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let currentIndex = subViewControllers.index(of: viewController){
            if currentIndex < subViewControllers.count-1{
                return subViewControllers[currentIndex+1]
            }
        }
        return nil
    }
}
