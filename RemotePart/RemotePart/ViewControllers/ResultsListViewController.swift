//
//  ResultsListViewController.swift
//  RemotePart
//
//  Created by CHANDRA SEKARAN M on 03/07/2017.
//  Copyright © 2017 CHANDRA SEKARAN M. All rights reserved.
//

import UIKit

class ResultsListViewController: UITableViewController, ResultListViewProtocol {

   // weak var viewModel: MainViewModel?
    weak var viewPresenter: MainPresenter?
    weak var navigationDelegate:NavigationDelegate?
    
    private var result:[ListData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadData()
        
       // self.notify(name: Constants.placesRefreshNotification, selector: #selector(ResultsListViewController.reloadData))
    }

    @objc func reloadData() {
        if let vm = viewPresenter{
            self.result = vm.getResultForList()
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.result?.count ?? 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceCell", for: indexPath)

        // Configure the cell...
        if  let r = result {
            let data = r[indexPath.row]
           // print("data:\(data)")
            cell.imageView?.imageFromServerURL(urlString: data.icon)
           // print(data.name)
            cell.textLabel?.text = data.name
            cell.detailTextLabel?.text = data.address
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if  let r = result , let navigation = self.navigationDelegate {
            let data = r[indexPath.row]
            
            if let vc = self.viewController(withIdentifier: "Details") as? PlaceDetailsViewController {
                vc.placeId = data.id
                
                navigation.navigateTo(view: vc)
            }
            
        }
    }
    
}
