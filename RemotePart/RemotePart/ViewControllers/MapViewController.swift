//
//  MapViewController.swift
//  RemotePart
//
//  Created by CHANDRA SEKARAN M on 03/07/2017.
//  Copyright © 2017 CHANDRA SEKARAN M. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: BaseViewController, MapViewProtocol
{

  //  weak var viewModel: MainViewModel?
    weak var viewPresenter: MainPresenter?
    private var result:[MapData]?
    
    private var pins = [PlaceAnnotation]()
    
    @IBOutlet weak var map: MKMapView!
    
    weak var navigationDelegate:NavigationDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadData()
        //self.notify(name: Constants.placesRefreshNotification, selector: #selector(MapViewController.reloadData))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadData()
    }
    
    @objc func reloadData() {
        print("inside reloadData")
        map.removeAnnotations(pins)
        
        if let vm = viewPresenter{
            self.result = vm.getResultForMap()
            guard let mapResult = self.result else{
                return
            }
            for mapdata in mapResult {
                let location = CLLocation(latitude: mapdata.lat, longitude: mapdata.lng)
                let pin = PlaceAnnotation(coordinate: location.coordinate ,title:mapdata.name)
                pin.place = mapdata
                pins.append(pin)
                //Banu
                self.map.addAnnotation(pin)
                print("Pin:" + "\(pin.place)")
                print("annotation added in map")
            }
        }
        if let firstPin = pins.first {
            map.setCenter(firstPin.coordinate,animated:false)
        }
    }


}

extension MapViewController:MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        print("----inside didselect method----")
        if let placeAnnotation = view.annotation as? PlaceAnnotation, let navigation = self.navigationDelegate {
            print(placeAnnotation.place)
            
            if let vc = self.viewController(withIdentifier: "Details") as? PlaceDetailsViewController {
                vc.placeId = placeAnnotation.place.id
                
                navigation.navigateTo(view: vc)
            }

        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        return nil
    }
}

class PlaceAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    
    fileprivate var place:MapData!
    
    init(coordinate: CLLocationCoordinate2D, title: String) {
        self.coordinate = coordinate
        self.title = title
    }
}
