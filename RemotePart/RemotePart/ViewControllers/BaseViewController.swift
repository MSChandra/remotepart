//
//  BaseViewController.swift
//  RemotePart
//
//  Created by CHANDRA SEKARAN M on 03/07/2017.
//  Copyright © 2017 CHANDRA SEKARAN M. All rights reserved.
//

import Foundation
import UIKit


@objc protocol NavigationDelegate {
    
    func navigateTo(view viewController:UIViewController)
}

class BaseViewController: UIViewController, BaseViewProtocol {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backItem?.title = ""
        
    }
    
  
    func showError(withMessage msg:String) {
        
        let alert = UIAlertController(title: "Error", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
extension UIViewController {
    func notify(name aName: NSNotification.Name?, selector aSelector: Selector){
        NotificationCenter.default.addObserver(self, selector: aSelector, name:aName, object: nil)
    }
    
    func viewController(withIdentifier identifier:String, storyBoard:String) -> UIViewController? {
        let sb = UIStoryboard(name: storyBoard, bundle: nil)
        
        guard let controller = sb.instantiateViewController(withIdentifier: identifier) as? BaseViewController else { return nil}
        
        return controller
    }
    
    func viewController(withIdentifier identifier:String) -> UIViewController? {
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        
        let controller = sb.instantiateViewController(withIdentifier: identifier)
        
        return controller
    }
}
