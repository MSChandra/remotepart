//
//  DirectionViewController.swift
//  RemotePart
//
//  Created by mbanu on 25/10/18.
//  Copyright © 2018 CHANDRA SEKARAN M. All rights reserved.
//

import UIKit

struct Locations{
    //let lat: Double
    //let long: Double
    /* init(lat: Double, long: Double) {
     self.lat = lat
     self.long = long
     }*/
   let coordinate:(lat:Double, long:Double)
    let maneuver: String?
    init(coordinate:(lat:Double, long:Double), maneuver:String){
        self.coordinate.lat = coordinate.lat
        self.coordinate.long = coordinate.long
        self.maneuver = maneuver
    }

}
class DirectionViewController: BaseViewController, DirectionViewProtocol{
    @IBOutlet weak var tableView: UITableView!
    var mode:String?
    
    @IBOutlet weak var directionLabel: UILabel!
    var source:String = "51.5193,0.0598"
    var destination:String = ""
    var waypoints = [String:Double]()
    var travelMode:String?
    @IBOutlet weak var segmentCtrl: UISegmentedControl!
    @IBOutlet var directionView: UIView!
    var locationCoordinatesFromModel = [Locations]()
    //longitude: "0.0598", latitude: "51.5193"
 //   var dirModel = DirectionViewModel()
    // MVP
    var presenter = DirectionPresenter()
    
    // MVP
    override func viewDidLoad() {
        super.viewDidLoad()
         // MVP
        presenter.presenterDelegate = self
        // MVP
       // self.notify(name: Constants.fetchDirectionSuccessNotification, selector: #selector(DirectionViewController.showDirection))
       // self.notify(name: Constants.fetchDirectionFailNotification, selector: #selector(DirectionViewController.showErr))
      }
    /*@objc func showDirection(){
        print("**************************************************")
        print("inside ShowDirection()")
        print("**************************************************")
        locationCoordinatesFromModel = dirModel.locationArray
        tableView.dataSource = self
        tableView.delegate = self
        DispatchQueue.main.async(execute: {
            self.tableView.reloadData()
        })
        
    }*/
    func showDirectionList(_ locations:[Locations]){
        print("**************************************************")
        print("inside ShowDirection()")
        print("**************************************************")
        locationCoordinatesFromModel = locations
        tableView.dataSource = self
        tableView.delegate = self
        DispatchQueue.main.async(execute: {
            self.tableView.reloadData()
        })
        
    }
    @objc func showErr(){
        self.showError(withMessage: "error in Direction")
    }
    func findRoute(){
          print("**************************************************")
          print("inside findRoute()")
          print("**************************************************")
          // MVP
          presenter.callDirectionAPI(source: source, destination: destination, travelMode: travelMode!)
          // MVP
          //dirModel.callDirectionAPI(source: source, destination: destination, travelMode: travelMode!)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        //travelMode = mode
        //print(travelMode)
        //findRoute()
        super.viewWillAppear(animated)
    
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       
    }
    @IBAction func selectTravelMode(_ sender: Any) {
        print("segment control clicked")
        let index = segmentCtrl.selectedSegmentIndex
        switch index {
        case 0:
            mode = "walking"
        case 1:
            mode = "driving"
        case 2:
            mode = "bicycling"
        case 3:
            mode = "transit"
        default:
            print("mode not selected")
        }
         travelMode = mode
        print(travelMode)
       findRoute()
        }
}
extension DirectionViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("numberOfRowsInSection called")
        return locationCoordinatesFromModel.count
    }
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "directioncell", for: indexPath)
        let latCoordinate = (locationCoordinatesFromModel[indexPath.row].coordinate.lat)
        let longCoordinate = locationCoordinatesFromModel[indexPath.row].coordinate.long
        let latString = NumberFormatter.localizedString(from: NSNumber(value:latCoordinate), number: .decimal)
        let longString = NumberFormatter.localizedString(from: NSNumber(value:longCoordinate), number: .decimal)
        cell.textLabel?.text = ("[ latitude:" + latString + ", Longitude:" + longString + "]")
        
        return cell
        }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

}
