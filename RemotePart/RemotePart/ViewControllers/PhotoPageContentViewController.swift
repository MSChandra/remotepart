//
//  PhotoPageContentViewController.swift
//  RemotePart
//
//  Created by mbanu on 13/09/18.
//  Copyright © 2018 CHANDRA SEKARAN M. All rights reserved.
//

import UIKit

class PhotoPageContentViewController: UIViewController {

    @IBOutlet weak var schoolImg: UIImageView!
   var parentPageview:PhotoPageViewController?
    var id:String?
    var photoRef:String?
    var photoRefURL:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        print("photoreference inside child vc")
        print(photoRef)
        if let myurl = photoRef {
        photoRefURL = Constants.placePhotosURL + photoRef!
        }
        else{
            photoRefURL = Constants.placePhotosURL
        }
        schoolImg.imageFromServerURL(urlString: photoRefURL!)
        // Do any additional setup after loading the view.
    }
 
    
}
