//
//  PlaceDetailsViewController.swift
//  RemotePart
//
//  Created by CHANDRA SEKARAN M on 04/07/2017.
//  Copyright © 2017 CHANDRA SEKARAN M. All rights reserved.
//

import UIKit
//Banu
var photoreference:String?

class PlaceDetailsViewController: BaseViewController, PlaceDetailsViewProtocol {

    @IBOutlet weak var placeImg: UIImageView!
    @IBOutlet weak var activityInd:UIActivityIndicatorView!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var reviewsText: UITextView!
    
    var placeId:String!
    var phref: String?
    
    //private let viewModel = PlaceDetailsViewModel()
    private let detailPresenter = PlaceDetailPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        detailPresenter.pdPresenterDelegate = self
        self.title = "Details"
       // self.notify(name: Constants.fetchPlaceDetailSuccessNotification, selector: #selector(PlaceDetailsViewController.showDetails))
        //self.notify(name: Constants.fetchPlaceDetailFailNotification, selector: #selector(PlaceDetailsViewController.showErr))
        self.activityInd.startAnimating()
        detailPresenter.fetchPlaceDetails(byId: placeId)
            }
    //Banu
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let receiverVC = segue.destination as? PhotoPageViewController{
        if let text = placeId{
         receiverVC.place = text
        }
        if let parentChildMapping = DBClient.sharedInstance.getRecentSearchResult(), let placesCorrespondingToSearchQuery = parentChildMapping.places{
            for item in placesCorrespondingToSearchQuery{
                if let placeTableItem = item as? Place {
                    if placeTableItem.placeId == self.placeId, let photosTableItems = placeTableItem.photos{ // photos corresponding to the particular placeId
                         //  print("photosTableItem")
                        //for phto in photosTableItem{
                          //  print("photo table")
                            //if let ph = phto as? Photo{
                                receiverVC.phReferences = photosTableItems.allObjects as? [Photo]
                            //}
                        }
                    }
                }
            }
        
      //receiverVC.phReference = photoreference
        receiverVC.phReferences?.first
        print("phReference inside prepare segue:" )
        print(receiverVC.phReferences)
        }
        else if let receiverVC = segue.destination as? DirectionViewController{
            print("Direction view controller called")
            if let text = placeId{
                receiverVC.destination = placeId
                }
        }
    }
    func showDetails(){
        self.activityInd.stopAnimating()
        
        if let details =  detailPresenter.datatToShow(){
            
            if let firstPhotoRef = details.photorefs.first{
            
            let URLofFirstPhoto = Constants.placePhotosURL + firstPhotoRef
            placeImg.imageFromServerURL(urlString: URLofFirstPhoto)
            }
            else{
                placeImg.imageFromServerURL(urlString: details.icon)
            }
            addressLabel.text =  details.address
            reviewsText.text = details.reviews
            //Banu
           photoreference = details.photorefs.first
           print("photoreference inside showdetail")
           print(details.photorefs.first)
           self.title = details.name == "" ? "Details" : details.name
            
        }else{
            self.showError(withMessage: "No data found")
        }
    }
    
    @objc func showErr(){
        
        self.activityInd.stopAnimating()
        self.showError(withMessage: "StartupFail".localizedText())
    }

}
