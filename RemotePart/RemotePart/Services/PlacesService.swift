//
//  PlacesService.swift
//  RemotePart
//
//  Created by CHANDRA SEKARAN M on 03/07/2017.
//  Copyright © 2017 CHANDRA SEKARAN M. All rights reserved.
//

import Foundation


class PlacesService : BaseService {


    init(success:@escaping SuccessJSONBlock, failure:@escaping FailureBlock){
        super.init()
        self.successCallBack = success
        self.failureCallBack = failure
    }
    
    func search(query:String) {
        
        let requestParms = ["query":query]
        
        self.api = Constants.searchPlacesURL
        
        setRequest(url: self.serviceURL(), method: .GET,params:requestParms, header: nil, body: nil)
        
        let webserviceClient = WebServiceClient()
        
        webserviceClient.callServer(requestContext: self.requestContext, successCall: self.serviceCallSuccess(), failureCall: self.serviceCallFail())
        
    }
    
    func searchNearBy(placeType type:String, longitude:String, latitude:String) {
        
        let requestParms = ["type":type, "location":latitude+","+longitude, "radius":"10000"]
        
        self.api = Constants.searchNearbyURL
        
        setRequest(url: self.serviceURL(), method: .GET,params:requestParms, header: nil, body: nil)
        
        let webserviceClient = WebServiceClient()
        
        webserviceClient.callServer(requestContext: self.requestContext, successCall: self.serviceCallSuccess(), failureCall: self.serviceCallFail())
    }
    
    func fetchPlaceDetails(byId placeId:String) {
        
        let requestParms = ["placeid":placeId]
        
        self.api = Constants.placeDetailsAPI
        
        setRequest(url: self.serviceURL(), method: .GET,params:requestParms, header: nil, body: nil)
        
        let webserviceClient = WebServiceClient()
        
        webserviceClient.callServer(requestContext: self.requestContext, successCall: self.serviceCallSuccess(), failureCall: self.serviceCallFail())
    }
}
