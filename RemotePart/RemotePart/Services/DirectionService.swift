//
//  DirectionService.swift
//  RemotePart
//
//  Created by mbanu on 16/11/18.
//  Copyright © 2018 CHANDRA SEKARAN M. All rights reserved.
//

import Foundation

class DirectionService : BaseService{
    init(success:@escaping SuccessJSONBlock, failure:@escaping FailureBlock) {
        super.init()
        self.successCallBack = success
        self.failureCallBack = failure
    }
    
    func getDirectionCoordinates(source:String, destination:String, travelmode tm:String){
        
        let requestParms = ["origin":source, "destination":"place_id:"+destination, "mode":tm]
        self.api = Constants.locationDirectionAPI
        setRequest(url: self.serviceURL(), method: .GET, params: requestParms, header: nil, body: nil)
        let webserviceClient = WebServiceClient()
        webserviceClient.callServer(requestContext: self.requestContext, successCall: self.serviceCallSuccess(), failureCall: self.serviceCallFail())
       
    }
}
