//
//  BaseService.swift
//  RemotePart
//
//  Created by CHANDRA SEKARAN M on 03/07/2017.
//  Copyright © 2017 CHANDRA SEKARAN M. All rights reserved.
//

import Foundation

class BaseService {
    
    var requestContext : RequestContext?
    
    var successCallBack: SuccessJSONBlock!
    var failureCallBack: FailureBlock!
    var api: String?
    var successCallBackForObj : [String:Any]?
    
    func setRequest(url:String , method:RequestMethod,params:Params?, header:Headers?, body:JSONDict?){
        self.requestContext = RequestContext(url: url, method: method, params:params, headers: header, body: body)
    }
    
    func serviceURL() -> String{
        guard let action = api else {
            return ""
        }
        
        return action
    }
    
    func serviceCallFail() -> FailureBlock  {
        
        return {(error) in
            self.failureCallBack(error)
        }
        
    }
    
    func serviceCallSuccess() -> SuccessDataBlock {
        return {
            (data,response) in
            
            let jsonResult:JSONDict? = SharedComponents.deserializedResponseData(data: data)
            print("---------------------------------------")
            print("Json result")
            print("---------------------------------------")
            print(jsonResult as Any)
                self.successCallBack(jsonResult)
        }
    }
    
}
