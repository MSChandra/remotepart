//
//  WebServiceClient.swift
//  RemotePart
//
//  Created by CHANDRA SEKARAN M on 03/07/2017.
//  Copyright © 2017 CHANDRA SEKARAN M. All rights reserved.
//

import Foundation

import Foundation

typealias Headers = [String:String]
typealias Params = [String:String]
typealias JSONDict = [String:AnyObject]
typealias SuccessJSONBlocknew = ([String:Any]) -> Void

typealias SuccessJSONBlock = (JSONDict?) -> Void

typealias SuccessDataBlock = (NSData?, URLResponse?) -> Void
typealias FailureBlock = (ResponseError?) -> Void


enum RequestMethod:String {
    
    case PUT = "PUT"
    case GET = "GET"
    case POST = "POST"
    
}
enum ResponseError : Error {
    case networkError
    case dataEmpty
    case urlInvalid
    case invalidInput
    case invalidResponse
}

struct RequestContext {
    var url: String
    var method:RequestMethod
    var params:[String:String]?
    var headers:Headers?
    var body:JSONDict?
    
    mutating func fullUrl() -> String{
        
        guard let reqParam = params else {
         //print("full url")
          // print(url)
            return url
        }
        
        switch (method){
        case RequestMethod.GET:
            var index = 0
            for (key,value) in reqParam {
                url = url + key + "=" + value
                if index < reqParam.keys.count - 1 {
                    url = url + "&"
                }
                index = index + 1
            }
            // print("***url inside 'RequestMethod.GET'******")
            //print(url)
            return url
        case RequestMethod.PUT, RequestMethod.POST:
            return url
        }
        
    }
}
extension URLRequest {
    
    mutating func setRequstHeader(params:Headers?){
        
        guard let headers = params else { return }
        
        for (name, value) in headers {
            self.addValue(value, forHTTPHeaderField: name)
        }
    }
    mutating func setRequstBody(params:JSONDict?){
        
        guard let bodyJson = params, self.httpMethod != RequestMethod.GET.rawValue else { return }
        
        
        let jsonData = try? JSONSerialization.data(withJSONObject: bodyJson)
        
        // insert json data to the request
        self.httpBody = jsonData
    }
    
}

class WebServiceClient {
    
    func callServer(requestContext: RequestContext?, successCall: @escaping SuccessDataBlock, failureCall: @escaping FailureBlock) {
        guard let requestCtx = requestContext else {
            failureCall(ResponseError.invalidInput)
            return
        }
        var mutabledRequest = requestCtx
        guard let components = NSURLComponents(string:mutabledRequest.fullUrl()), let url = components.url
            else {
                failureCall(.urlInvalid)
                return
        }
        let session = URLSession(configuration: URLSessionConfiguration.default)
        print ("___fullurl___")
        print(url)
        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setRequstHeader(params: mutabledRequest.headers)
        request.httpMethod = mutabledRequest.method.rawValue
        if mutabledRequest.method != RequestMethod.GET {
            request.setRequstBody(params: mutabledRequest.body)
        }
        let task = session.dataTask(with: request, completionHandler: { (data, response, networkError) -> Void in
            
            guard networkError == nil else {
                failureCall(.networkError)
                return
            }
            
            guard let jsonData = data else {
                failureCall(.dataEmpty)
                return
            }
            
            successCall(jsonData as NSData, response)
            
        })
        task.resume()
    }
    
}
