//
//  MainViewModel.swift
//  RemotePart
//
//  Created by CHANDRA SEKARAN M on 03/07/2017.
//  Copyright © 2017 CHANDRA SEKARAN M. All rights reserved.
//

import Foundation
import MapKit

typealias ListData = (id:String , name:String, address:String, icon:String)
typealias MapData = (id:String , lng:Double, lat:Double)

class MainViewModel : BaseViewModel {
    
    fileprivate var searchQuery:String?
    
    fileprivate var searchNearbyFlag = false
    
    let locationManager = CLLocationManager()
    
    func searchPlaces(forQuery query:String) {
        self.searchQuery = query
        let service = PlacesService(success: searchPlacesCallSuccess(),failure: searchPlacesCallFail() )
        service.search(query:query)
        
    }
    func searchPlacesCallSuccess() -> SuccessJSONBlock {
        return {
            [weak self](json) in
            if let query = self?.searchQuery {
                let _ = DBClient.sharedInstance.addSearchResult(forQuery: query, data: PlaceMetaData.createPlaces(fromJson: json))
            }
            self?.postNotification(name: Constants.fetchPlacesSuccessNotification, info: nil)
        }
    }
    
    
    func searchPlacesCallFail() -> FailureBlock  {
        
        return {[weak self] (error) in
            self?.postNotification(name: Constants.fetchPlacesSuccessNotification, info: nil)
        }
        
    }

    func getResultForList() -> [ListData] {
        
        var items = [ListData]()
        
        if let queryResultMapping = DBClient.sharedInstance.getRecentSearchResult(), let result = queryResultMapping.places {
            for item in result {
                if let place = item as? Place, let id = place.placeId , let name = place.name, let address = place.address, let icon = place.icon {
                    items.append(ListData(id:id, name:name, address:address,icon: icon))
                }
            }
        }
        return items
    }
    
    func getResultForMap() -> [MapData] {
        
        var items = [MapData]()
        
        if let queryResultMapping = DBClient.sharedInstance.getRecentSearchResult(), let result = queryResultMapping.places {
            for item in result {
                if let place = item as? Place, let id = place.placeId {
                    items.append(MapData(id:id,  lng: place.lng, lat:place.lat))
                }
            }
        }
        return items
    }
    
    func searchNearByPubs() {
        self.locationManager.requestWhenInUseAuthorization()
        self.searchQuery = "bar"
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            searchNearbyFlag = true
        }
    }
    
}
extension MainViewModel :CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let locValue = manager.location{
            let coordinate = locValue.coordinate
            print("locations = \(coordinate.latitude),\(coordinate.longitude)")
            locationManager.stopUpdatingLocation()
            if searchNearbyFlag {
                searchNearbyFlag = false
                let service = PlacesService(success: searchPlacesCallSuccess(),failure: searchPlacesCallFail() )
                service.searchNearBy(placeType: "bar", longitude: "\(coordinate.longitude)", latitude: "\(coordinate.latitude)")
            }
        }
    }

}
