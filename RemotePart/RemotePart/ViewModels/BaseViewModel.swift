//
//  BaseViewModel.swift
//  RemotePart
//
//  Created by CHANDRA SEKARAN M on 03/07/2017.
//  Copyright © 2017 CHANDRA SEKARAN M. All rights reserved.
//

import Foundation

class BaseViewModel:NSObject {
    
    
    func postNotification(name:NSNotification.Name, info:Any?){
        DispatchQueue.main.async(execute: {
            NotificationCenter.default.post(name: name, object: info)
        })
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
