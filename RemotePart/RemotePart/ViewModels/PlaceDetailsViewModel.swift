//
//  PlaceDetailsViewModel.swift
//  RemotePart
//
//  Created by CHANDRA SEKARAN M on 04/07/2017.
//  Copyright © 2017 CHANDRA SEKARAN M. All rights reserved.
//

import UIKit

typealias PlaceDetails = (name:String, icon:String, reviews:String, address:String)

class PlaceDetailsViewModel: BaseViewModel {
    
    var details : PlaceMetaData?
    
    func fetchPlaceDetails(byId placeId:String) {
        let service = PlacesService(success: searchPlaceDetailsCallSuccess(),failure: searchPlaceDetailsCallFail() )
        service.fetchPlaceDetails(byId: placeId)
        
    }
    func searchPlaceDetailsCallSuccess() -> SuccessJSONBlock {
        return {
            [weak self](json) in
            
            self?.details = PlaceMetaData.createPlacesDetail(fromJson: json)
            self?.postNotification(name: Constants.fetchPlaceDetailSuccessNotification, info: nil)
        }
    }
    
    
    func searchPlaceDetailsCallFail() -> FailureBlock  {
        
        return {[weak self] (error) in
            self?.postNotification(name: Constants.fetchPlaceDetailFailNotification, info: nil)
        }
        
    }
    
    func datatToShow() -> PlaceDetails?{
        guard let dts = details else {
            return nil
        }
        
        return PlaceDetails(name:dts.name ?? "", icon:dts.imageUrl ?? "noname", reviews:dts.reviews.joined(separator: "\n"), address:dts.address ?? "")
    }
}
