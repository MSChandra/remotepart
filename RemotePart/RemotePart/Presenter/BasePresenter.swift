//
//  BasePresenter.swift
//  RemotePart
//
//  Created by mbanu on 10/01/19.
//  Copyright © 2019 CHANDRA SEKARAN M. All rights reserved.
//

import Foundation
class BasePresenter:NSObject {
    
    
    func postNotification(name:NSNotification.Name, info:Any?){
        DispatchQueue.main.async(execute: {
            NotificationCenter.default.post(name: name, object: info)
        })
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
