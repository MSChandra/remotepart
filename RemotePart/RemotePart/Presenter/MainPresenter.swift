//
//  MainPresenter.swift
//  RemotePart
//
//  Created by mbanu on 10/01/19.
//  Copyright © 2019 CHANDRA SEKARAN M. All rights reserved.
//

import Foundation
import MapKit

typealias ListData = (id:String , name:String, address:String, icon:String)
typealias MapData = (id:String , name:String, lng:Double, lat:Double)

class MainPresenter : BasePresenter {
    weak var mainDelegate:MainViewProtocol?
    fileprivate var searchQuery:String?
    
    fileprivate var searchNearbyFlag = false
    
    let locationManager = CLLocationManager()
    
    func searchPlaces(forQuery query:String) {
        self.searchQuery = query
        let service = PlacesService(success: searchPlacesCallSuccess(),failure: searchPlacesCallFail() )
        service.search(query:query)
        
    }
    func searchPlacesCallSuccess() -> SuccessJSONBlock {
        return {
            [weak self](json) in
            if let query = self?.searchQuery {
                let _ = DBClient.sharedInstance.addSearchResult(forQuery: query, data: PlaceMetaData.createPlaces(fromJson: json))
            }
            //self?.postNotification(name: Constants.fetchPlacesSuccessNotification, info: nil)
            self?.mainDelegate?.refreshResults()
        }
    }
    
    
    func searchPlacesCallFail() -> FailureBlock  {
        
        return {[weak self] (error) in
           // self?.postNotification(name: Constants.fetchPlacesFailNotification, info: nil)
            self?.mainDelegate?.showErr()
        }
        
    }
    
    func getResultForList() -> [ListData] {
        
        var items = [ListData]()
        
        if let queryResultMapping = DBClient.sharedInstance.getRecentSearchResult(), let result = queryResultMapping.places{
            for item in result {
                if let place = item as? Place, let id = place.placeId , let name = place.name, let address = place.address, let icon = place.icon, let ph = place.photos {
                    items.append(ListData(id:id, name:name, address:address,icon: icon))
                    
                }
            }
        }
        return items
    }
    
    func getResultForMap() -> [MapData] {
        
        var items = [MapData]()
        
        if let queryResultMapping = DBClient.sharedInstance.getRecentSearchResult(), let result = queryResultMapping.places {
            for item in result {
                if let place = item as? Place, let id = place.placeId, let name = place.name {
                    items.append(MapData(id:id, name:name, lng: place.lng, lat:place.lat))
                }
            }
        }
        return items
    }
    
    func searchNearByPubs() {
        self.locationManager.requestWhenInUseAuthorization()
        self.searchQuery = "school"
        print("before locationservice check")
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self as! CLLocationManagerDelegate
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            searchNearbyFlag = true
            print("location service enabled")
        }
        if searchNearbyFlag {
            searchNearbyFlag = false
            let service = PlacesService(success: searchPlacesCallSuccess(),failure: searchPlacesCallFail() )
            service.searchNearBy(placeType: "school", longitude: "0.0598", latitude: "51.5193")
        }
    }
    
}
extension MainPresenter :CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
}
