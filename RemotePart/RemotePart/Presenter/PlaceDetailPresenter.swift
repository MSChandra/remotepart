//
//  PlaceDetailPresenter.swift
//  RemotePart
//
//  Created by mbanu on 09/01/19.
//  Copyright © 2019 CHANDRA SEKARAN M. All rights reserved.
//

import UIKit
typealias PlaceDetails = (name:String, icon:String, reviews:String, address:String, photorefs:[String])

class PlaceDetailPresenter : BasePresenter {
     weak var pdPresenterDelegate:PlaceDetailsViewProtocol?
var details : PlaceMetaData?

func fetchPlaceDetails(byId placeId:String) {
    let service = PlacesService(success: searchPlaceDetailsCallSuccess(),failure: searchPlaceDetailsCallFail() )
    service.fetchPlaceDetails(byId: placeId)
    
}
func searchPlaceDetailsCallSuccess() -> SuccessJSONBlock {
    return {
        [weak self](json) in
        
        self?.details = PlaceMetaData.createPlacesDetail(fromJson: json)
        guard let placeId = self?.details?.placeId, let photos = self?.details?.photoReferences else { return }
        DBClient.sharedInstance.addPhotos(forPlace: placeId , photos: photos)
        
        //self?.postNotification(name: Constants.fetchPlaceDetailSuccessNotification, info: nil)
        DispatchQueue.main.async {
            self?.pdPresenterDelegate?.showDetails()
        }
    }
}


func searchPlaceDetailsCallFail() -> FailureBlock  {
    
    return {[weak self] (error) in
        //self?.postNotification(name: Constants.fetchPlaceDetailFailNotification, info: nil)
        self?.pdPresenterDelegate?.showErr()
    }
    
}

func datatToShow() -> PlaceDetails?{
    guard let dts = details else {
        return nil
    }
    
    return PlaceDetails(name:dts.name ?? "", icon:dts.imageUrl ?? "noname", reviews:dts.reviews.joined(separator: "\n"), address:dts.address ?? " ", photorefs:dts.photoReferences)
}
}

