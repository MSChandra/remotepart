//
//  DirectionPresenter.swift
//  RemotePart
//
//  Created by Chandra on 08/01/2019.
//  Copyright © 2019 CHANDRA SEKARAN M. All rights reserved.
//

import UIKit

class DirectionPresenter: NSObject {
    // MVP
    weak var presenterDelegate:DirectionViewProtocol?
    // MVP
    
    var modeljson:JSONDict?
    var locationArray = [Locations]()
    func callDirectionAPI(source: String, destination: String, travelMode: String){
        let dirService = DirectionService(success: searchDirectionCallSuccess(),failure: searchDirectionCallFail())
        dirService.getDirectionCoordinates(source: source, destination: destination, travelmode: travelMode)
    }
    func searchDirectionCallSuccess()-> SuccessJSONBlock{
        return {
            [weak self](json) in
            let routes = json!["routes"] as? [Any]
            //print(routes)
            for route in routes!{
                if let path = route as? [String:Any], let legsInRoutes = path["legs"] as? [Any]{
                    //print(legsInRoutes)
                    for legs in legsInRoutes{
                        if let leg = legs as? [String:Any], let stepsInLeg = leg["steps"] as? [Any]{
                            // print(stepsInLeg)
                            for steps in stepsInLeg{
                                if let step = steps as? [String:Any]{
                                    let startcoordinate = step["start_location"] as? [String:Double], mymaneuver = step["maneuver"] as? String
                                    //print(startcoordinate)
                                    let latitude = startcoordinate!["lat"]
                                    let longitude = startcoordinate!["lng"]
                                    //print(locations.init(coordinate: (lat: latitude!, long: longitude!)))
                                    self?.locationArray.append(Locations(coordinate: (lat: latitude!, long: longitude!), maneuver: mymaneuver ?? ""))
                                }
                            }
                        }
                    }
                }
            }
            // self?.postNotification(name: Constants.fetchDirectionSuccessNotification, info: nil)
            
            
            // MVP
            if let locations = self?.locationArray {
                self?.presenterDelegate?.showDirectionList(locations)
            }
            // MVP
        }
    }
    func searchDirectionCallFail()-> FailureBlock{
        return {[weak self] (error) in
            // MVP
            self?.presenterDelegate?.showErr()
            // MVP
        }
    }
    
}
