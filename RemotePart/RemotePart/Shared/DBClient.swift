//
//  DBClient.swift
//  RemotePart
//
//  Created by CHANDRA SEKARAN M on 03/07/2017.
//  Copyright © 2017 CHANDRA SEKARAN M. All rights reserved.
//

import Foundation


import CoreData

class DBClient {
    
    static let sharedInstance = DBClient()
    
    
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "RemotePart")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    func addPhotos(forPlace: String, photos:[String]) {
        
        self.getRecentSearchResult()?.places?.forEach({ (place) in
            if let placeObj = place as? Place, let placeId =  placeObj.placeId, placeId == forPlace {
                photos.forEach { (ref) in
                   // let fetch:NSFetchRequest<Photo> = Photo.fetchRequest()
                    //let results =  try context.fetch(fetch) as [Photo]
                    //for record in results
                    //{
                    //}
                        // print("results.first inside getSearchResult")
                    var available = false
                    let context = persistentContainer.viewContext
                    do {
                        let fetch:NSFetchRequest<Photo> = Photo.fetchRequest()
                        let records =  try context.fetch(fetch) as [Photo]
                        for record in records{
                            if record.photoDetail == ref{
                                 available = true
                            }
                        }
                    } catch {
                        print("Fetching Failed")
                    }
                    if available == false{
                    let photo = Photo(context: persistentContainer.viewContext)
                    photo.photoDetail = ref
                    placeObj.addToPhotos(photo)
                    }
                }
            }
        })
        saveContext()
    }
    
    func addSearchResult(forQuery query:String, data:[PlaceMetaData]) -> SearchResult? {
        
        cleanAll()
        
        let searchResult = SearchResult(context: persistentContainer.viewContext)
        
        for placeMeta in data{
            let place = Place(context: persistentContainer.viewContext)
            place.address = placeMeta.address
           // print("address added")
           // print(place.address)
            place.icon = placeMeta.imageUrl
            place.lat = placeMeta.location?.latitude.doubleValue ?? 0
            place.lng = placeMeta.location?.longitude.doubleValue ?? 0
            place.types = placeMeta.types?.joined(separator: ",")
            place.placeId = placeMeta.placeId
            place.name = placeMeta.name
            //Banu
            //print("phtodetail added")
            //print(place.photodetail)
            searchResult.addToPlaces(place)
            //print("data added in Place Table")
            //Banu
            placeMeta.photoReferences.forEach { (ref) in
                let photo = Photo(context: persistentContainer.viewContext)
                photo.photoDetail = ref
                place.addToPhotos(photo)
            }
            
            //print("data added in Photo Table")
        }
        
        searchResult.key = query
        
        saveContext()
        
        return getSearchResult(identifier: query)
    }
    
    
    func cleanAll(){
        if let results = getAllSearchResults(){
            
            for result in results {
                if let places = result.places {
                    for place in places {
                        if let placeDataObj = place as? Place {
                            result.removeFromPlaces(placeDataObj)
                            
                        }
                    }
                }
                persistentContainer.viewContext.delete(result)
            }
        }
    }
    func getAllSearchResults() -> [SearchResult]?{
        
        
        let context = persistentContainer.viewContext
        do {
            let fetch:NSFetchRequest<SearchResult> = SearchResult.fetchRequest()
            
            let result =  try context.fetch(fetch) as [SearchResult]
            return result
        } catch {
            print("Fetching Failed")
        }
        return nil
    }
    
    func getSearchResult(identifier id:String?) -> SearchResult?{
        
guard let identifier = id else {
            return nil
        }
        let context = persistentContainer.viewContext
        do {
            let fetch:NSFetchRequest<SearchResult> = SearchResult.fetchRequest()
            
            fetch.predicate = NSPredicate( format: "key == %@",identifier)
            let results =  try context.fetch(fetch) as [SearchResult]
            
            if results.count > 0 {
               // print("results.first inside getSearchResult")
                //print(results.first)
                return results.first
            }
        } catch {
            print("Fetching Failed")
        }
        return nil
    }
    
    func getRecentSearchResult() -> SearchResult?{
        
        let context = persistentContainer.viewContext
        do {
            let fetch:NSFetchRequest<SearchResult> = SearchResult.fetchRequest()
            
            let results =  try context.fetch(fetch) as [SearchResult]
            
            if results.count > 0 {
                return results.first
            }
        } catch {
            print("Fetching Failed")
        }
        return nil
    }
   
    
}
