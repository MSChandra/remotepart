//
//  Constants.swift
//  RemotePart
//
//  Created by CHANDRA SEKARAN M on 03/07/2017.
//  Copyright © 2017 CHANDRA SEKARAN M. All rights reserved.
//

import Foundation

struct Constants {
    
    static let APIKey = "AIzaSyD3wNupZlaULT4V8iZgt2lW5IotlEw8IB0"
    static let searchNearbyURL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=\(APIKey)&"
    static let searchPlacesURL = "https://maps.googleapis.com/maps/api/place/textsearch/json?key=\(APIKey)&"
    static let placeDetailsAPI = "https://maps.googleapis.com/maps/api/place/details/json?key=\(APIKey)&"
    //Banu
    static let placePhotosURL = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&key=\(APIKey)&photoreference="
    static let locationDirectionAPI = "https://maps.googleapis.com/maps/api/directions/json?key=\(APIKey)&"
    static let fetchDirectionSuccessNotification = Notification.Name("FetchDirectionSuccess")
    static let fetchDirectionFailNotification = Notification.Name("FetchDirectionFail")
    
    static let fetchPlacesFailNotification = Notification.Name("FetchPlacesFail")
    static let fetchPlacesSuccessNotification = Notification.Name("FetchPlacesSuccess")
    
    static let fetchPlaceDetailFailNotification = Notification.Name("FetchPlaceDetailFail")
    static let fetchPlaceDetailSuccessNotification = Notification.Name("FetchPlaceDetailSuccess")
    
    static let placesRefreshNotification = Notification.Name("PlacesRefresh")
    
}
