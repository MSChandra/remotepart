//
//  SharedComponents.swift
//  RemotePart
//
//  Created by CHANDRA SEKARAN M on 03/07/2017.
//  Copyright © 2017 CHANDRA SEKARAN M. All rights reserved.
//

import Foundation
import UIKit


class Cache {
    static let instance  = Cache()
    
    var images = [String:Data]()
        
}
class SharedComponents {
    
     class func deserializedResponseData(data:NSData?) ->JSONDict? {
        do {
            if let rawData = data, let jsonResult = try JSONSerialization.jsonObject(with: rawData as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:AnyObject]
            {
                return jsonResult
            }
            return nil
        } catch {
            return nil
        }
    }
}

extension UIImageView {
    
    public func imageFromServerURL(urlString: String) {
        
        let noimage = "noimage"
    //    print("************ url inside imageFromServerURL ***********")
     //   print(urlString)
        if urlString == ""
        {
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(named: noimage)
                self.image = image
                return
            })
        }else {
            
            if let data = Cache.instance.images[urlString]
            {
                let image = UIImage(data: data)
                self.image = image
                return
            }
            
            URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
                
                if error != nil
                {
                    DispatchQueue.main.async(execute: { () -> Void in
                        let image = UIImage(named: noimage)
                        self.image = image
                    })
                    return
                }
                DispatchQueue.main.async(execute: { () -> Void in
                    if let imgData = data{
                        Cache.instance.images[urlString] = imgData
                        let image = UIImage(data: imgData)
                        self.image = image
                    }
                })
                
            }).resume()
        }
    }
}

extension String {
    func localizedText() ->String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
extension String {
    
    var html2AttributedString: NSAttributedString? {
        guard
          let data = data(using: String.Encoding.utf8)
            else { return nil }
        do {
          //return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute:NSNumber(value:String.Encoding.utf8.rawValue)], documentAttributes: nil)
          return try NSAttributedString(data: Data(utf8),
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)

        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    
}
