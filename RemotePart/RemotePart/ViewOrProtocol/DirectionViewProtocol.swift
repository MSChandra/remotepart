//
//  DirectionView.swift
//  RemotePart
//
//  Created by mbanu on 27/12/18.
//  Copyright © 2018 CHANDRA SEKARAN M. All rights reserved.
//

import Foundation
protocol DirectionViewProtocol :  NSObjectProtocol{
   // func showDirection()
    func showDirectionList(_ locations:[Locations])
    func showErr()
    func findRoute()
}
