//
//  MapView.swift
//  RemotePart
//
//  Created by mbanu on 28/12/18.
//  Copyright © 2018 CHANDRA SEKARAN M. All rights reserved.
//

import Foundation
protocol MapViewProtocol {
    func reloadData()
}
