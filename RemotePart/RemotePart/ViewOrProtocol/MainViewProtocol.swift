//
//  MainView.swift
//  RemotePart
//
//  Created by mbanu on 27/12/18.
//  Copyright © 2018 CHANDRA SEKARAN M. All rights reserved.
//

import Foundation
protocol MainViewProtocol:NSObjectProtocol{
    func refreshResults()
    func showErr()
    //pending tabchanged() method
    }
