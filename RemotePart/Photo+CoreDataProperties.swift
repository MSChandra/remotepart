//
//  Photo+CoreDataProperties.swift
//  RemotePart
//
//  Created by mbanu on 05/10/18.
//  Copyright © 2018 CHANDRA SEKARAN M. All rights reserved.
//
//

import Foundation
import CoreData


extension Photo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Photo> {
        return NSFetchRequest<Photo>(entityName: "Photo")
    }

    @NSManaged public var photoDetail: String?

}
